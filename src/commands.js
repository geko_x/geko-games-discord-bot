// Run node ./src/commands.ts when updating slash commands
var fs = require('fs');
var REST = require('@discordjs/rest').REST;
var Routes = require('discord-api-types/v9').Routes;
var _a = require('../config.json'), token = _a.token, clientId = _a.clientId, serverId = _a.serverId, roleAdmin = _a.roleAdmin;
var commands = [];
var commandFiles = fs.readdirSync('./src/commands').filter(function (file) { return file.endsWith('.ts'); });
for (var _i = 0, commandFiles_1 = commandFiles; _i < commandFiles_1.length; _i++) {
    var file = commandFiles_1[_i];
    var command = require("./commands/" + file)["default"];
    if (command.type == "slash")
        commands.push(command.data.toJSON());
}
var rest = new REST({ version: '9' }).setToken(token);
rest.put(Routes.applicationGuildCommands(clientId, serverId), { body: commands })
    .then(function () { return console.log('Successfully registered application commands.'); })["catch"](console.error);
