import { SlashCommandBuilder } from "@discordjs/builders";
import { ApplicationCommand, ApplicationCommandManager, ApplicationCommandResolvable, ButtonInteraction, Client, Collection, CommandInteraction, Guild, GuildMember, Intents, Interaction, SelectMenuInteraction, TextChannel } from "discord.js";
const { roleAdmin } = require("../config.json");
const fs = require("fs");


export class GekoBot {
	
	static instance: GekoBot;
	
	token: string;
	serverId: string;
	clientId: string;
	guild: Guild;

	public client = new Client({ intents: [Intents.FLAGS.GUILDS] })
	public commands: Collection<string, any> = new Collection();
	public fetchedCommands: Collection<string, ApplicationCommand<{}>> = new Collection();
	
	public buttons: Collection<string, any> = new Collection();
	public menus: Collection<string, any> = new Collection();

	constructor(token, serverId, clientId) {
		
		if(GekoBot.instance != null) {
			throw("An instance of GekoBot has already been created");
		}

		GekoBot.instance = this;
		
		this.token = token;
		this.serverId = serverId;
		this.clientId = clientId;
	}

	public async Login() {

		await this.registerCommands();
		await this.registerEvents();

		console.log("Logging in");
		this.client.login(this.token);
	}

	private async registerCommands() {

		console.log("Registering commands...");

		this.commands = new Collection();

		// Find all commands in /commands and add them to the commands
		const commandFiles = fs.readdirSync('./src/commands').filter(file => file.endsWith('.ts'));
		for (const file of commandFiles) {
			const command = require(`./commands/${file}`).default;

			switch (command.type) {
				case 'slash':
					this.commands.set(command.data.name, command);
					console.log(`Registered ${command.data.name} ${command.data.description}`);
					break;
				
				case 'button':
					this.buttons.set(command.customId, command);
					console.log(`Registered button ${command.customId}`);
					break;
				
				case 'menu':
					this.menus.set(command.customId, command);
					console.log(`Registered menu ${command.customId}`);
					break;
				
				default:
					console.log(`Command does not have a specified type.`);
					console.log(command);
					break;
			}

			// if(command.needsAdmin === true) {
			// 	const commandId = await this.client.application.commands.fetch()
			// 	this.client.application.commands.permissions.set()
			// }
		}

		console.log("Done");
	}

	private async registerEvents() {

		console.log("Registering events...");

		this.client.once("ready", async () => {
			this.OnReady();
		});
		this.client.on("interactionCreate", async (interaction) => {
			this.OnInteractionCreate(interaction);
		});
	}

	// =========
	// Events
	// =========

	async OnReady() {
		console.log(`Ready! Logged in as ${this.client.user.tag}`);
		this.guild = this.client.guilds.cache.get(this.serverId);

		// // Update permissions
		// this.fetchedCommands = await this.guild.commands.fetch();

		// this.commands.forEach(async c => {

		// 	console.log(c);

		// 	if (c.needsAdmin) {
		// 		console.log("Updating command %s with admin permissions", c.data.name);
		// 		const slashCommand = this.fetchedCommands.find(command => command.name === c.data.name);
		// 		const cmdId = slashCommand.id;

		// 		const p = [{
		// 			id: roleAdmin as string,
		// 			type: 'ROLE',
		// 			permission: true,
		// 		}];

		// 		slashCommand.permissions.add({ p });

		// 		console.log(cmdId);

		// 		const fullPermissions = [
		// 			{
		// 				id: cmdId,
		// 				permissions: [
		// 					{
		// 						id: roleAdmin as string,
		// 						type: 'ROLE',
		// 						permission: true,
		// 					},
		// 					{
		// 						id: this.guild.roles.everyone.id,
		// 						type: 'ROLE',
		// 						permission: false,
		// 					}
		// 				]
		// 			},
		// 		];

		// 		await this.guild.commands.permissions.set({ fullPermissions });
		// 	}
		// });
	}

	async OnInteractionCreate(interaction: Interaction) {
		if (interaction.isCommand()) {
			await this.handleSlashCommand(interaction as CommandInteraction);
			return;
		}

		if (interaction.isButton()) {
			await this.handleButton(interaction as ButtonInteraction);
			return;
		}

		if (interaction.isSelectMenu()) {
			await this.handleSelectionMenu(interaction as SelectMenuInteraction);
			return;
		}
	}

	async handleSlashCommand(interaction: CommandInteraction) {

		const channel = interaction.channel as TextChannel;
		const user = interaction.user;
		const member = interaction.member as GuildMember;

		console.log(`${user.tag} triggered an interaction /${interaction.commandName} in #${channel.name}.`);

		const command = this.commands.get(interaction.commandName) as any;
		if (!command) return;
		
		let canExecute = false;

		if (command.needsAdmin) {
			if(member.roles.cache.find(id => id.id == roleAdmin)) {
				canExecute = true;
			}
		}

		else {
			canExecute = true;
		}

		if(canExecute) {
			try {
				await command.execute(interaction);
			} catch (error) {
				console.error("Command failed: " + error);
				await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
			}
		}

		else {
			console.log(`${user.tag} attempted to run command without permission.`);
			await interaction.reply({ content: 'You do not have permission to use this command!', ephemeral: true });
		}
	}

	async handleButton(interaction: ButtonInteraction) {
		const channel = interaction.channel as TextChannel;
		const user = interaction.user;
		const member = interaction.member as GuildMember;

		console.log(`${user.tag} triggered a button ${interaction.customId} in #${channel.name}.`);

		const button = this.buttons.get(interaction.customId) as any;
		if (!button) {
			console.log(`Button ${interaction.id} was not found!`);
			return;
		}

		try {
			await button.execute(interaction);
		}
		catch (error) {
			console.error("Button failed: " + error);
			await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
		}


		//await interaction.reply({ content: 'Boop', ephemeral: true });

	}

	async handleSelectionMenu(interaction: SelectMenuInteraction) {
		const channel = interaction.channel as TextChannel;
		const user = interaction.user;
		const member = interaction.member as GuildMember;

		console.log(`${user.tag} triggered a menu selection ${interaction.customId} in #${channel.name}.`);

		const menu = this.menus.get(interaction.customId) as any;
		if (!menu) {
			console.log(`Menu ${interaction.id} was not found!`);
			return;
		}

		try {
			await menu.execute(interaction);
		}
		catch (error) {
			console.error("Menu failed: " + error);
			if(interaction.replied)
				await interaction.followUp({ content: 'There was an error while executing this command!', ephemeral: true });
			else
				await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
		}
	}

}