import { TeamMemberMembershipState } from "discord-api-types";
import { Guild, GuildEmoji, GuildMember, GuildStickerManager, Role } from "discord.js";

const { customIdRoleSelection, roleMa, roleUnity } = require("../../config.json");

export default {
	type: "menu",
	customId: customIdRoleSelection,
	async execute(interaction) {
		
		async function getRole(guild: Guild, roleName: string): Promise<Role> {
			const role = await guild.roles.cache.find(role => role.name == roleName);
			if (role == undefined) {
				console.log(`Role ${roleName} was not found!`);
			}

			return role;
		}

		async function checkRole(member: GuildMember, guild: Guild, roleName: string) {
			const role = await getRole(guild, roleName);

			if (values.includes(roleName)) {
				await setRole(member, role);
			}

			else {
				await removeRole(member, role);
			}
		}

		async function setRole(member: GuildMember, role: Role) {
			await member.roles.add(role.id);
		}

		async function removeRole(member: GuildMember, role: Role) {
			await member.roles.remove(role.id);
		}
		
		const member = interaction.member as GuildMember;
		const guild = interaction.guild as Guild;
		const values = interaction.values as string[];

		await interaction.reply({ content: "Updating roles", ephemeral: true });

		await checkRole(member, guild, "ma-player");
		await checkRole(member, guild, "unity");

		const updatedRoles: Array<string> = [];
		await member.fetch(true);
		const r = member.roles.cache;
		r.forEach(r => {
			updatedRoles.push(r.name);
		});

		// Remove @everyone
		const i = updatedRoles.indexOf("@everyone");
		if (i > -1) {
			updatedRoles.splice(i, 1);
		}

		await interaction.editReply({
			content: `Your new roles are ${updatedRoles.join(", ")}`,
			ephemeral: true
		});
	}
};