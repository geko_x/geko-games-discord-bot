const { SlashCommandBuilder} = require('@discordjs/builders');
const { MessageActionRow, MessageButton, MessageSelectMenu } = require('discord.js');
const { customIdRoleSelection } = require("../../config.json");

export default {
	type: "slash",
	data: new SlashCommandBuilder()
		.setName('createrolebuttons')
		.setDescription('Displays the role assignment prompts'),
	
	async execute(interaction) {
		
		console.log("Creating role assingments");

		// const row = new MessageActionRow()
		// 	.addComponents(
		// 		new MessageButton()
		// 			.setCustomId(customIdRoleSelection + ".ma")
		// 			.setLabel('Mech Annihilation')
		// 			.setStyle('PRIMARY')
		// 			.setEmoji("<:ma:904566045040324618>"),
				
		// 		new MessageButton()
		// 			.setCustomId(customIdRoleSelection + ".unity")
		// 			.setLabel('Unity')
		// 			.setStyle('PRIMARY')
		// 			.setEmoji("<:unity:904545666615898192>"),
				
		// 	);

		const row = new MessageActionRow()
			.addComponents(
				new MessageSelectMenu()
					.setCustomId(customIdRoleSelection)
					.setPlaceholder("Nothing selected")
					.setMinValues(0)
					.setMaxValues(2)
					.addOptions([
						{
							label: "Mech Annihilation",
							description: "Choose this if you want to view channels about Mech Annihilation",
							value: "ma-player"
						},
						{
							label: "Unity Asset Store",
							description: "Choose this if you want to view support channels for Unity Asset store assets",
							value: "unity"
						}
					])
			)
		
		await interaction.reply({
			// content: "Access the rest of the server with these buttons! \n"
			// 	+ "<:ma:904566045040324618> For all Mech Annihilation channels.\n"
			// 	+ "<:unity:904545666615898192> For all Unity asset help and support.\n",
			content: "Select role",
			components: [row],
			ephemeral: false
		});

	},
	needsAdmin: true,
};