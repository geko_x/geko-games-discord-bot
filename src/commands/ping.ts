const { SlashCommandBuilder } = require('@discordjs/builders');

export default {
	type: "slash",
	data: new SlashCommandBuilder()
		.setName('ping')
		.setDescription('Replies with Pong!'),
	async execute(interaction) {
		await interaction.reply({ content: 'Pong', ephemeral: true });
	},
	needsAdmin: false,
};