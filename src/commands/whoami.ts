const { SlashCommandBuilder } = require('@discordjs/builders');

export default {
	type: "slash",
	data: new SlashCommandBuilder()
		.setName('whoami')
		.setDescription('Replies with your user info')
		.setDefaultPermission(true),
	async execute(interaction) {
		await interaction.reply({
			content: `Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}`,
			ephemeral: true,
		});
	},
	needsAdmin: false,
};
