import { GekoBot } from "./gekobot";

// Require the necessary discord.js classes
const { Client, Collection, Intents } = require("discord.js");
// const { REST } = require("@discordjs/rest");
// const { Routes } = require("discord-api-types/v9");

const { token, serverId, clientId } = require("../config.json");

const bot = new GekoBot(token, serverId, clientId);
bot.Login();